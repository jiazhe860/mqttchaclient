﻿using MQTTClient.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MQTTClient.Bll
{
    /// <summary>
    /// 数据体发送内容 业务类
    /// </summary>
    public class PayloadBll
    {       
        /// <summary>
        /// 获得对象JSON串
        /// </summary>
        /// <param name="pd"></param>
        /// <returns></returns>
        public string  GetPayloadDataJSONStr_ByOBJ<T>(T pd)
        {
            T clientdata =pd;
            return ObjectToJsonStr(clientdata);
             
        }
        /// <summary>
        /// 根据字符串获得对象
        /// </summary>
        /// <param name="pd"></param>
        /// <returns></returns>
        public T GetOBJ_ByPayloadDataJSONStr<T>(T type, string jsonStr)
        {
            return JsonStrToObject(type, jsonStr);            
        }
      
         
        /// <summary>
        /// 将对象转换为JSON字符串
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public string ObjectToJsonStr<T>(T  obj)
        {
            string JsonStr = JsonConvert.SerializeObject(obj);
            return JsonStr;
        }

        public T JsonStrToObject<T>(T type, string jsonstr)
        {
            type = JsonConvert.DeserializeObject<T>(jsonstr);
            return type;
        }
    }
}
