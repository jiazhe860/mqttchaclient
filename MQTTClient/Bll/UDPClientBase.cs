﻿using MQTTClient.Model;
using MQTTClient.Model.UDPEvent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace MQTTClient.Bll
{
    public class UDPClientBase
    {
        protected UdpState UdpState;
        protected UdpClient udpClient;
        public int ServerPort = 4600;
        public int ClientProt = 0;
        public string ServerIP = string.Empty;
        public string ClientId = string.Empty;
        // 本机IP节点
        public IPEndPoint ClientIPEndPoint;
        // 服务器IP节点
        public IPEndPoint ServerIPEndPoint;

        /// <summary>
        /// 连接服务器事件
        /// </summary>
        public event EventHandler<ServerConnectdEventArgs> ServerConnectdEvent;
        /// <summary>
        /// 发送数据事件
        /// </summary>
        public event EventHandler<ClientDataEventArgs> SendDatadEvent;
        /// <summary>
        /// 接收事件
        /// </summary>
        public event EventHandler<ClientDataEventArgs> ReceivedDataEvent;
        /// <summary>
        /// 产生错误事件
        /// </summary>
        public event EventHandler<MyExceptionEventArgs> MyExceptionEvent;


        /// <summary>
        /// 触发服务器连接事件的方法
        /// </summary>
        /// <param name="err"></param>
        public virtual void OnClientConnectd(IPEndPoint Point, string message)
        {
            EventHandler<ServerConnectdEventArgs> handler = this.ServerConnectdEvent;
            if (handler != null)
            {
                ServerConnectdEventArgs e = new ServerConnectdEventArgs(Point, message);
                handler(this, e);
            }
        }
        /// <summary>
        /// 发送数据事件
        /// </summary>
        /// <param name="clientData"></param>
        protected virtual void OnServerSendDatad(string clientData)
        {
            EventHandler<ClientDataEventArgs> handler = this.SendDatadEvent;
            if (handler != null)
            {
                ClientDataEventArgs e = new ClientDataEventArgs(clientData);
                handler(this, e);
            }
        }
        /// <summary>
        /// 接收数据事件
        /// </summary>
        /// <param name="clientData"></param>
        protected virtual void OnServerReceived(string clientData)
        {
            EventHandler<ClientDataEventArgs> handler = this.ReceivedDataEvent;
            if (handler != null)
            {
                ClientDataEventArgs e = new ClientDataEventArgs(clientData);
                handler(this, e);
            }
        }
        /// <summary>
        /// 接收或发送字符串数据事件
        /// </summary>
        //protected virtual void OnReceived_SendMessageDatad(string mesgData)
        //{
        //    EventHandler<ClientMessageDataEventArgs> handler = this.ReceivedMessageDataEvent;
        //    if (handler != null)
        //    {
        //        ClientMessageDataEventArgs e = new ClientMessageDataEventArgs(mesgData);
        //        handler(this, e);
        //    }
        //}
        /// <summary>
        /// 产生错误事件
        /// </summary>
        /// <param name="ex"></param>
        protected virtual void OnClientExceptiond(string str, Exception ex)
        {
            EventHandler<MyExceptionEventArgs> handler = this.MyExceptionEvent;
            if (handler != null)
            {
                MyExceptionEventArgs e = new MyExceptionEventArgs(str, ex);
                handler(this, e);
            }
        }

        protected virtual void OnClientExceptiond(string str, string errorMesg)
        {
            EventHandler<MyExceptionEventArgs> handler = this.MyExceptionEvent;
            if (handler != null)
            {
                MyExceptionEventArgs e = new MyExceptionEventArgs(str+errorMesg);
                handler(this, e);
            }
        }
    }
}
