﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MQTTClient.Model
{
    /// <summary>
    /// 客户集合
    /// </summary>
    public class ClientListModel
    {
        private List<ClientPayloadDataModel> clist = new List<ClientPayloadDataModel>();
        public List<ClientPayloadDataModel> ClientList
        {
            get { return clist; }
            set { clist = value; }
        }
    }
}
