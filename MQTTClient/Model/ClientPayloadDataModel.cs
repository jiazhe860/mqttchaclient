﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MQTTClient.Model
{
   public  class ClientPayloadDataModel
    {
        List<ClientPayloadDataModel> clientList = new List<ClientPayloadDataModel>();
        private string mesg = string.Empty;
        /// <summary>
        /// 客户端自己的名称
        /// </summary>
        public string ClientName
        {
            get;
            set;
        }
        /// <summary>
        /// 发送消息的客户端名称
        /// </summary>
        public string SendDataClientName
        {
            get;
            set;
        }
        public string ClinetIpProt
        {
            get;
            set;
        }
        /// <summary>
        /// 获取的消息数据内容
        /// </summary>
        public string DataMessage
        {
            get { return mesg; }
            set { mesg = value; }
        }
        /// <summary>
        /// 客户集合，客户数为0时，发送的是单个客户数据，不为0时代表发送的客户列表数据
        /// </summary>
        public List<ClientPayloadDataModel> ClientList
        {
            get { return clientList; }
            set { clientList = value; }
        }
        /// <summary>
        /// 客户数据类型，1为添加客户  2为客户掉线
        /// </summary>
        public int ClientDataType
        {
            get;
            set;
        }
        /// <summary>
        /// 是否私聊
        /// </summary>
        public bool IsSiLiao
        {
            get;
            set;
        }
        /// <summary>
        /// 接收消息的时间
        /// </summary>
        public string DataMessageTime
        {
            get;
            set;
        }
        string onlineTime;
        public string OnLineTime
        {
            get;
            set;
        }
        /// <summary>
        /// 最后接收数据时间
        /// </summary>
        public DateTime LastReceiveDataTime
        {
            get;
            set;
        }
    }
}
