﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MQTTClient.Model
{
    /// <summary>
    /// 统一的消息体，作为发送数据对象
    /// </summary>
    public class PayloadDataBase
    {
        /// <summary>
        /// 业务类型 0.私有消息 暂无意义;1.所有客户端名称; 2.单个客户端上线;3.公共聊天数据;
        /// </summary>
        public int PayloadType
        {
            get;
            set;
        }
        /// <summary>
        /// 数据体内容
        /// </summary>
        public string DataContent
        {
            get;
            set;
        }
       
    }
}
