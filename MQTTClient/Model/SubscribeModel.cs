﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MQTTClient.Model
{
    public class SubscribeModel
    {
        /// <summary>
        /// 公共订阅主题，各客户端上线后统一订阅此主题。用于各客户端之后保持相互通信。
        /// </summary>
        public static string PublicTopic = "PublicTopic";
    }
}
