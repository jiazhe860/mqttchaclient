﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MQTTClient.Model.UDPEvent
{
    public class ClientDataEventArgs
    {      
        /// <summary>
        /// 服务器连接事件参数
        /// </summary>
        /// <param name="Point"></param>
        /// <param name="message"></param>
        public ClientDataEventArgs(string ClientData)
        {
            this.ClientData = ClientData;
        }
        /// <summary>
        /// 获得当前Session对象
        /// </summary>
        public string ClientData
        {
            get;
            set;
        }

    }
}
