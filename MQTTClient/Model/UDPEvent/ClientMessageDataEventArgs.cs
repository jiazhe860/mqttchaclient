﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MQTTClient.Model.UDPEvent
{
    public class ClientMessageDataEventArgs
    {
        private string mesg;
        public ClientMessageDataEventArgs(string mesg)
        {
            this.mesg = mesg;
        }
        public ClientMessageDataEventArgs(ClientPayloadDataModel Cobj)
        {
            ClientOBJ = Cobj;
        }
        public string MessageData
        {
            get { return mesg; }
        }
        public ClientPayloadDataModel ClientOBJ
        {
            get;
            set;
        }
    }
}
