﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MQTTClient.Model.UDPEvent
{
    public class MyExceptionEventArgs : EventArgs
    {
        private string m_exceptionMessage;

        public MyExceptionEventArgs(Exception exception)
        {
            m_exceptionMessage = exception.Message;
        }
        public MyExceptionEventArgs(string str, Exception exception)
        {
            m_exceptionMessage = str + "：" + exception.Message;
        }

        public MyExceptionEventArgs(string exceptionMessage)
        {
            m_exceptionMessage = exceptionMessage;
        }

        public string ExceptionMessage
        {
            get { return m_exceptionMessage; }
        }
    }
}
