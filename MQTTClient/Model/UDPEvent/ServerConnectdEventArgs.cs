﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MQTTClient.Model.UDPEvent
{
    public class ServerConnectdEventArgs
    {
        private IPEndPoint ipEndPoint;
        private string mesg;
        public ServerConnectdEventArgs(IPEndPoint ipEndPoint, string mesg)
        {
            this.ipEndPoint = ipEndPoint;
            this.mesg = mesg;
        }

        public IPEndPoint IPEndPoint
        {
            get;
            set;
        }
        public string Message
        {
            get { return mesg; }
        }
    }
}
