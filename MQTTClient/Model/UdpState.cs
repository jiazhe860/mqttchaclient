﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace MQTTClient.Model
{
    public class UdpState
    {
        /// <summary>
        /// 客户端UDP对象
        /// </summary>
        public UdpClient udpClient;
        public IPEndPoint IpEndPoint;
        public const int BufferSize = 1024;
        public byte[] buffer = new byte[BufferSize];
        public int counter = 0;

    }
}
